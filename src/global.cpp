#include <global.h>

QSettings * global::settings = nullptr;

QString global::d_application = "\0";

bool global::setApplicationDirectory(QString p_application) {
	if (global::d_application != "\0")
		return false;
	QString d_application = util::getDirectory(p_application);
	global::d_application = d_application;
	return true;
}

QString global::getApplicationDirectory() {
	return d_application;
}
