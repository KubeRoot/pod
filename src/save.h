#ifndef SAVE_H
#define SAVE_H

#include <savemap.h>

namespace save {
	static char const * const DEFAULT_SAVE = "{}";
	std::string convertVanillaSave(std::string d_vanilla = "\0");
}

#endif //SAVE_H
