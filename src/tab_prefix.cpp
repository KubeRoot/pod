#include <tab_prefix.h>

#include <prefix.h>
#include <links.h>
#include <util.h>

#include <QLabel>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QStringListModel>
#include <QFileDialog>

QList<QString> tab_prefix::getPrefixes() {
	QList<QString> prefixes;

	if (prefixmanager && prefixmanager->valid())
		for (prefix::Prefix & p : prefixmanager->getPrefixes())
			prefixes.push_back(QString::fromStdString(p.path));

	return prefixes;
}

tab_prefix::tab_prefix() {
	QVBoxLayout * layout_main = new QVBoxLayout;

	QHBoxLayout * layout_vanilla = new QHBoxLayout;

	QHBoxLayout * layout_prefix = new QHBoxLayout;

	QVBoxLayout * layout_buttons = new QVBoxLayout;

	QStringListModel * model = new QStringListModel;
	view_prefix = new QListView;
	view_prefix->setModel(model);
	view_prefix->setSelectionMode(QAbstractItemView::SingleSelection);
	view_prefix->setEditTriggers(QAbstractItemView::NoEditTriggers);

	connect(view_prefix->selectionModel(), &QItemSelectionModel::currentChanged, this, &tab_prefix::prefixSelected);

	QPushButton * button_create = new QPushButton("Create");
	connect(button_create, &QPushButton::clicked, this, &tab_prefix::selectedCreate);
	layout_buttons->addWidget(button_create);

	button_update = new QPushButton("Check Update");
	connect(button_update, &QPushButton::clicked, this, &tab_prefix::selectedUpdate);
	layout_buttons->addWidget(button_update);

	layout_prefix->addWidget(view_prefix);
	layout_prefix->addLayout(layout_buttons);

	QLabel * label_vanilla = new QLabel("Vanilla:");
	line_vanilla = new QLineEdit();
	line_vanilla->setReadOnly(true);
	QPushButton * button_vanilla = new QPushButton("...");

	connect(button_vanilla, &QPushButton::clicked, [this] () {
		prefixmanager->grabVanillaDirectory(this);
		refreshVanilla();
	});

	layout_vanilla->addWidget(label_vanilla);
	layout_vanilla->addWidget(line_vanilla);
	layout_vanilla->addWidget(button_vanilla);

	layout_main->addLayout(layout_vanilla);
	layout_main->addLayout(layout_prefix);

	this->setLayout(layout_main);
}

void tab_prefix::selectedUpdate() {
	QList<QModelIndex> selected = view_prefix->selectionModel()->selectedIndexes();

	if (selected.size() != 1) {
		QMessageBox::information(this, "Check Update", "Select a prefix first");
		return;
	}

	QString d_prefix = selected.at(0).data().toString();

	if (!prefix::isPrefixDirectory(d_prefix.toStdString())) {
		QMessageBox::information(this, "Check Update", "Selected prefix is not valid");
		return;
	}

	prefixmanager->installRainfusion(d_prefix, false, false, this);
}

bool tab_prefix::setPrefixManager(PrefixManager * prefixmanager) {
	this->prefixmanager = prefixmanager;
	refreshVanilla();
	refreshPrefixes();
	return true;
}

bool tab_prefix::refreshPrefixes() {
	qobject_cast<QStringListModel *>(view_prefix->model())->setStringList(getPrefixes());
	setPrefix(current_prefix);
	return true;
}

void tab_prefix::selectedCreate() {
	QString d_start;
	if (prefixmanager->valid())
		d_start = prefixmanager->getVanillaDirectory();
	QString directory = QFileDialog::getExistingDirectory(this, "Select prefix installation directory", d_start);
	
	if (directory == "")
		return;
	
	directory = util::getDirectory(directory);
	std::string d_given = directory.toStdString();

	if (!QDir().mkpath(directory))
		return;

	prefixmanager->installRainfusion(directory, false, false, this);
}

void tab_prefix::prefixSelected(QModelIndex const & index, QModelIndex const & parent) {
	QString path = index.data().toString();
	emit changedCurrentPrefix(path);
}

void tab_prefix::setPrefix(prefix::Prefix current_prefix) {
	this->current_prefix = current_prefix;

	QString name = QString::fromStdString(current_prefix.path);
	QAbstractItemModel * model = view_prefix->model();
	for (size_t i = 0; i < model->rowCount(); i++) {
		if (model->index(i, 0).data().toString() == name) {
			QItemSelectionModel * selection_model = view_prefix->selectionModel();
			selection_model->select(model->index(i, 0), QItemSelectionModel::ClearAndSelect);
			break;
		}
	}
}

bool tab_prefix::refreshVanilla() {
	if (line_vanilla && prefixmanager && prefixmanager->validVanilla()) {
		line_vanilla->setText(prefixmanager->getVanillaDirectory());
		return true;
	}
	return false;
}
