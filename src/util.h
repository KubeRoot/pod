#ifndef UTIL_H
#define UTIL_H

#include <regex>

#include <QFileInfo>
#include <QMessageBox>

namespace util {
	std::regex const clean_regex("[^[:alnum:][:space:]-_+]");
	inline std::string cleanString(std::string s) {
		std::string value = std::string(std::regex_replace(s, clean_regex, ""));
		if (value.empty())
			return "unknown";
		return value;
	}
	inline QString getDirectory(QString path) {
		if (path.isEmpty())
			return QString();
		QFileInfo info(path);
		return info.isDir() ? info.absoluteFilePath() : info.absolutePath();
	}
	inline bool question(QWidget * widget, QString title, QString text) {
		auto answer = QMessageBox::question(widget, title, text, QMessageBox::Yes | QMessageBox::No);
		return answer == QMessageBox::Yes;
	}
}

#endif //UTIL_H
