#ifndef PROFILE_H
#define PROFILE_H

#include <vector>

// TODO
#include <QString>

namespace profile {
	bool isProfileDirectory(std::string directory);

	class profile_data {
		public:
			profile_data();
			
			std::vector<std::string> enabled_mods;
			std::vector<std::string> flags;
	};

	class ProfileFile {
		public:
			ProfileFile();
			ProfileFile(QString f_profile, bool read_now = false);

			bool read();
			bool write();
			bool valid();

			QString getName();

			QString f_profile;
			profile_data data;
	};
}

#endif //PROFILE_H
