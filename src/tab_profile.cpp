#include <tab_profile.h>

#include <save.h>
#include <util.h>

#include <algorithm>

#include <QFile>
#include <QDir>
#include <QBoxLayout>
#include <QLabel>
#include <QPushButton>
#include <QStringListModel>
#include <QSettings>
#include <QMessageBox>
#include <QInputDialog>

// TODO
#include <QtCore>

tab_profile::tab_profile(QWidget * parent) : QWidget(parent) {
	QHBoxLayout * layout_main = new QHBoxLayout;
	
	// Profiles

	QVBoxLayout * layout_profiles = new QVBoxLayout;
	QHBoxLayout * layout_profiles_buttons = new QHBoxLayout;

	QLabel * label_profiles = new QLabel("Profiles");
	label_profiles->setAlignment(Qt::AlignCenter);

	QPushButton * button_refresh = new QPushButton("Refresh");
	connect(button_refresh, &QPushButton::clicked, this, &tab_profile::refreshProfiles);

	QPushButton * button_create_profile = new QPushButton("Create");
	QPushButton * button_delete_profile = new QPushButton("Delete");
	connect(button_create_profile, SIGNAL(clicked()), this, SLOT(createProfile()));
	connect(button_delete_profile, SIGNAL(clicked()), this, SLOT(deleteProfile()));
	
	layout_profiles_buttons->addWidget(button_create_profile);
	layout_profiles_buttons->addWidget(button_delete_profile);

	QStringListModel * model_profiles = new QStringListModel;
	view_profiles = new QListView;
	view_profiles->setSelectionMode(QAbstractItemView::SingleSelection);
	view_profiles->setEditTriggers(QAbstractItemView::NoEditTriggers);
	view_profiles->setModel(model_profiles);

	connect(view_profiles->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)), this, SLOT(profileSelected(const QModelIndex &, const QModelIndex &)));

	layout_profiles->addWidget(label_profiles);
	layout_profiles->addWidget(view_profiles);
	layout_profiles->addWidget(button_refresh);
	layout_profiles->addLayout(layout_profiles_buttons);

	// FLags

	QVBoxLayout * layout_flags = new QVBoxLayout;
	QHBoxLayout * layout_flags_buttons = new QHBoxLayout;

	QLabel * label_flags = new QLabel("Flags");
	label_flags->setAlignment(Qt::AlignCenter);

	QPushButton * button_create_flag = new QPushButton("Create");
	QPushButton * button_delete_flag = new QPushButton("Delete");

	connect(button_create_flag, SIGNAL(clicked()), this, SLOT(createFlag()));
	connect(button_delete_flag, SIGNAL(clicked()), this, SLOT(deleteFlag()));
	
	layout_flags_buttons->addWidget(button_create_flag);
	layout_flags_buttons->addWidget(button_delete_flag);

	QStringListModel * model_flags = new QStringListModel;
	view_flags = new QListView;
	view_flags->setSelectionMode(QAbstractItemView::ExtendedSelection);
	view_flags->setEditTriggers(QAbstractItemView::DoubleClicked);
	view_flags->setModel(model_flags);

	connect(view_flags->model(), SIGNAL(dataChanged(QModelIndex const &, QModelIndex const &, QVector<int> const &)), this, SLOT(editedFlag(QModelIndex const &, QModelIndex const &, QVector<int> const &)));
	
	layout_flags->addWidget(label_flags);
	layout_flags->addWidget(view_flags);
	layout_flags->addLayout(layout_flags_buttons);

	// Saves

	QVBoxLayout * layout_saves = new QVBoxLayout;
	QHBoxLayout * layout_saves_buttons = new QHBoxLayout;

	QLabel * label_saves = new QLabel("Saves");
	label_saves->setAlignment(Qt::AlignCenter);

	QPushButton * button_import_save = new QPushButton("Import");
	connect(button_import_save, &QPushButton::clicked, this, &tab_profile::selectedImportSave);
	layout_saves_buttons->addWidget(button_import_save);

	QPushButton * button_blank_save = new QPushButton("New Blank Save");
	connect(button_blank_save, &QPushButton::clicked, this, &tab_profile::clickedBlankSave);
	layout_saves_buttons->addWidget(button_blank_save);

	layout_saves->addWidget(label_saves);
	layout_saves->addLayout(layout_saves_buttons);

	// Main Layout

	layout_main->addLayout(layout_profiles);
	layout_main->addLayout(layout_flags);
	layout_main->addLayout(layout_saves);

	this->setLayout(layout_main);
}

void tab_profile::setPrefix(prefix::Prefix current_prefix) {
	this->current_prefix = current_prefix;
	refreshProfiles();
}

bool isProfileDir(QString d_profile) {
	if (QFile(d_profile + "/profile.json").exists())
		return true;
	return false;
}

profile::ProfileFile profileFromDir(QString d_profile) {
	return profile::ProfileFile(d_profile + "/profile.json", true);
}

void tab_profile::refreshProfiles() {
	available_profiles = std::vector<profile::ProfileFile>();

	QString path = QString::fromStdString(current_prefix.path) + "/profiles";

	QStringList profile_dirs = QDir(path).entryList(QStringList(), QDir::Dirs);
	for (int i = 0; i < profile_dirs.size(); i++) {
		QString profile_dir = profile_dirs.at(i);
		if (profile_dir == "." || profile_dir == "..")
			continue;
		profile_dir = path + "/" + profile_dir;
		if (isProfileDir(profile_dir)) {
			available_profiles.push_back(profileFromDir(profile_dir));
		}
	}

	QStringList qprofiles;
	for (int i = 0; i < available_profiles.size(); i++) {
		qprofiles.push_back(available_profiles.at(i).getName());
	}
	QStringListModel * model_profiles = qobject_cast<QStringListModel *>(view_profiles->model());
	model_profiles->setStringList(qprofiles);

	//current_profile = prefix::getCurrentProfile(current_prefix.path);
	//refreshCurrentProfile();
	//setProfile(current_profile);
	setProfile(prefix::getCurrentProfile(current_prefix.path));
}

profile::ProfileFile tab_profile::getProfile(QString name) {
	for (int i = 0; i < available_profiles.size(); i++) {
		if (available_profiles.at(i).getName() == name) {
			return available_profiles.at(i);
		}
	}
	return profile::ProfileFile();
}

void tab_profile::refreshCurrentProfile() {
	if (!current_profile.valid())
		return;

	QStringListModel * model_flags = qobject_cast<QStringListModel *>(view_flags->model());
	
	QStringList qflags;
	std::vector<std::string> const & flags = current_profile.data.flags;
	for (int i = 0; i < flags.size(); i++) {
		qflags.push_back(QString::fromStdString(flags.at(i)));
	}

	model_flags->setStringList(qflags);
}

void tab_profile::profileSelected(QModelIndex const & index, QModelIndex const & parent) {
	QString name = index.data().toString();
	current_profile = getProfile(name);
	refreshCurrentProfile();
	emit changedCurrentProfile(current_profile);
}

void tab_profile::createFlag() {
	current_profile.data.flags.push_back("(double click to change)");
	current_profile.write();
	refreshCurrentProfile();
}

void tab_profile::deleteFlag() {
	if (QMessageBox::question(this, "Profiles", "Delete selected flags?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
		return;

	std::vector<std::string> & flags = current_profile.data.flags;

	QList<QModelIndex> selected = view_flags->selectionModel()->selectedIndexes();
	for (int i = 0; i < selected.size(); i++) {
		std::string name = selected.at(i).data().toString().toStdString();
		flags.erase(std::find(flags.begin(), flags.end(), name));
	}

	current_profile.write();
	refreshCurrentProfile();
}

void tab_profile::setProfile(profile::ProfileFile profile) {
	current_profile = profile;
	current_profile.read();
	QString name = profile.getName();
	QAbstractItemModel * model = view_profiles->model();
	for (size_t i = 0; i < model->rowCount(); i++) {
		if (model->index(i, 0).data().toString() == name) {
			QItemSelectionModel * selection_model = view_profiles->selectionModel();
			selection_model->setCurrentIndex(model->index(i, 0), QItemSelectionModel::ClearAndSelect);
			refreshCurrentProfile();
			break;
		}
	}
}

void tab_profile::editedFlag(QModelIndex const & topleft, QModelIndex const & bottomright, QVector<int> const & roles) {
	//if (topleft == bottomright)
	QStringList new_flags = qobject_cast<QStringListModel *>(view_flags->model())->stringList();
	current_profile.data.flags = std::vector<std::string>();
	std::vector<std::string> & flags = current_profile.data.flags;
	for (int i = 0; i < new_flags.size(); i++) {
		flags.push_back(new_flags.at(i).toStdString());
	}
	current_profile.write();
	refreshCurrentProfile();
}

void tab_profile::createProfile() {
	bool status;
	QString profile_name = QInputDialog::getText(this, "Create Profile", "Profile Name:", QLineEdit::Normal, "", &status);

	if (!status || profile_name.isEmpty()) {
		QMessageBox::information(this, "Create Profile", "No profile name specified");
		return;
	}

	current_prefix.createProfile(util::cleanString(profile_name.toStdString()));

	emit changedProfiles();

	refreshProfiles();
}

void tab_profile::deleteProfile() {
	QItemSelectionModel * selection_model = view_profiles->selectionModel();

	// TODO profile.size instead of hasSelection?
	if (!selection_model->hasSelection()) {
		QMessageBox::information(this, "Delete Profile", "No profile selected");
		return;
	}

	QList<QModelIndex> profiles = selection_model->selectedIndexes();

	// Confirm
	QString profile_string;
	for (size_t i = 0; i < profiles.size(); i++) {
		if (i)
			profile_string += ", ";
		profile_string += profiles.at(i).data().toString();
	}
	if (QMessageBox::question(this, "Delete Profile", "Are you sure you want to delete these profiles: " + profile_string + "?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes) {
		return;
	}

	for (size_t i = 0; i < profiles.size(); i++) {
		current_prefix.deleteProfile(profiles.at(i).data().toString().toStdString());
	}

	emit changedProfiles();

	refreshProfiles();
}

bool tab_profile::saves_init() {
	if (!current_prefix.valid()) {
		qInfo() << "Current prefix is not valid. Cannot initialize saves.";
		return false;
	}

	QString d_saves = QString::fromStdString(current_prefix.path + "/saves");
	if (!QDir().mkpath(d_saves)) {
		qInfo() << "Can not access or create saves directory for current prefix.";
		return false;
	}

	return true;
}

// TODO
void tab_profile::selectedImportSave() {
	if (!saves_init()) {
		return;
	}

	QString d_saves = QString::fromStdString(current_prefix.path + "/saves");
	QFile savefile(d_saves + "/Default.json");

	if (savefile.exists()) {
		if (QMessageBox::question(this, "Import Save", "Override existing save file?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
			return;
	}

	if (!savefile.open(QIODevice::WriteOnly)) {
		qInfo() << "Couldn't open savefile";
		return;
	}

	savefile.write(QString::fromStdString(save::convertVanillaSave(d_vanilla)).toUtf8());
	savefile.close();

	QMessageBox::information(this, "Import Save", "Vanilla savefile imported");
}

void tab_profile::clickedBlankSave() {
	if (!saves_init()) {
		return;
	}

	QString d_saves = QString::fromStdString(current_prefix.path + "/saves");
	QFile savefile(d_saves + "/Default.json");

	if (savefile.exists()) {
		if (QMessageBox::question(this, "Import Save", "Override existing save file?", QMessageBox::Yes | QMessageBox::No) != QMessageBox::Yes)
			return;
	}

	if (!savefile.open(QIODevice::WriteOnly)) {
		qInfo() << "Couldn't open savefile";
		return;
	}

	savefile.write(QString(save::DEFAULT_SAVE).toUtf8());
	savefile.close();

	QMessageBox::information(this, "New Blank Save", "New blank save created.");
}
