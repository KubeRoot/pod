#ifndef TAB_MODS_H
#define TAB_MODS_H

#include <mod.h>
#include <modview.h>
#include <modlistmodel.h>
#include <profile.h>
#include <prefix.h>

#include <vector>

#include <QWidget>
#include <QPushButton>
#include <QStringListModel>
#include <QJsonArray>
#include <QTreeView>
#include <QListView>

class tab_mods : public QWidget {
	Q_OBJECT

	public:
		tab_mods(QWidget * parent = nullptr);

	public:
		void refreshMods();
		void applyEnabled();
		void setPrefix(prefix::Prefix current_prefix);
		void setProfile(profile::ProfileFile);
		bool profileError(bool box);

	public slots:
		void enableSelection();
		void disableSelection();
		void mod_activated(QModelIndex const & index, QModelIndex const & previous_index);
		void openModsDir();
	
	private:
		prefix::Prefix current_prefix;
		profile::ProfileFile current_profile;

		QPushButton * button_enable = nullptr;
		QPushButton * button_disable = nullptr;
		QTreeView * view_mods = nullptr;
		std::vector<mod::ModPath> mods;
		ModView * mod_view = nullptr;
		ModListModel * modlist_model;

		QJsonArray enabled_mods;
};

#endif //TAB_MODS_H
