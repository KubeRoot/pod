#ifndef GLOBAL_H
#define GLOBAL_H

#include <util.h>

#include <QString>
#include <QSettings>

namespace global {
	extern QSettings * settings;
	extern QString d_application;
	bool setApplicationDirectory(QString p_application);
	QString getApplicationDirectory();
}

#endif //GLOBAL_H
