#include <save.h>

#include <vanilla.h>

// TODO
#include <QtCore>

#include <QFile>
#include <QSettings>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>

namespace save {
	bool convertVanillaSection(std::map<std::string, std::string> const & keymap, QSettings const * from, QJsonObject * to, int threshold = 0) {
		std::map<std::string, std::string>::const_iterator it = keymap.begin();
		while (it != keymap.end()) {
			std::string original = it->first;
			std::string mapped = it->second;
			if (from->contains(QString::fromStdString(original))) {
				QString svalue = from->value(QString::fromStdString(original)).toString();
				if (svalue.startsWith('"')) {
					svalue = svalue.right(svalue.size() - 1);
					svalue = svalue.left(svalue.size() - 1);
				}
				double value = svalue.toDouble();
				// Doesn't work
				//(*to)[QString::fromStdString(mapped)] = (threshold != 0) ? (bool)(value >= (double)threshold) : value;
				if (threshold > 0)
					(*to)[QString::fromStdString(mapped)] = (value >= (double)threshold);
				else
					(*to)[QString::fromStdString(mapped)] = value;
			}
			it++;
		}
		return true;
	}

	std::string convertVanillaSave(std::string d_vanilla) {
		if (!vanilla::isVanillaDirectory(d_vanilla))
			return DEFAULT_SAVE;

		QString f_vanilla_save = QString::fromStdString(d_vanilla + "/Save.ini");

		if (!QFile(f_vanilla_save).exists()) {
			qInfo() << "Save couldn't be found";
			return DEFAULT_SAVE;
		}

		QSettings vanilla_save(f_vanilla_save, QSettings::IniFormat);

		QJsonObject root;
		QJsonObject item;
		QJsonObject artifact;
		QJsonObject monster;
		QJsonObject stats;
		QJsonObject games;
		QJsonObject achievement;

		vanilla_save.beginGroup("Record");
		convertVanillaSection(save::ITEMS, &vanilla_save, &item, 1);
		convertVanillaSection(save::ARTIFACTS, &vanilla_save, &artifact, 1);
		convertVanillaSection(save::MONSTERS, &vanilla_save, &monster, 1);
		convertVanillaSection(save::STATS, &vanilla_save, &stats);
		convertVanillaSection(save::STATSGAMES, &vanilla_save, &games);
		vanilla_save.endGroup();

		vanilla_save.beginGroup("Achievement");
		convertVanillaSection(save::ACHIEVEMENTS, &vanilla_save, &achievement);
		vanilla_save.endGroup();

		root["item"] = item;
		root["artifact"] = artifact;
		root["monster"] = monster;
		root["achievement"] = achievement;

		stats["games"] = games;
		root["stats"] = stats;

		QJsonDocument document(root);

		return QString(document.toJson()).toStdString();
	}
}
