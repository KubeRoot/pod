#include <tab_tasks.h>

#include <QBoxLayout>
#include <QHeaderView>

tab_tasks::tab_tasks(QWidget * parent) : QWidget(parent) {
	QVBoxLayout * layout_main = new QVBoxLayout;

	// Take ownership of model with (this) so that the tasks are canceled on quitting
	model = new TaskModel(this);

	view_tasks = new QTreeView;
	view_tasks->setItemsExpandable(false);
	view_tasks->setRootIsDecorated(false);
	view_tasks->setUniformRowHeights(true);
	view_tasks->header()->setStretchLastSection(false);
	view_tasks->header()->setSectionResizeMode(QHeaderView::Stretch);
	view_tasks->setSelectionMode(QAbstractItemView::NoSelection);
	view_tasks->setModel(model);
	view_tasks->setItemDelegate(new TaskViewDelegate(this));
	//connect(model, SIGNAL(dataChanged(QModelIndex const &, QModelIndex const &)), view_tasks, SLOT(dataChanged(QModelIndex const &, QModelIndex const &)));

	button_clear = new QPushButton("Clear");
	connect(button_clear, &QPushButton::clicked, this, &tab_tasks::clearFinished);

	layout_main->addWidget(view_tasks);
	layout_main->addWidget(button_clear);

	this->setLayout(layout_main);
}

void tab_tasks::clearFinished() {
	model->clearCompleted();
}
