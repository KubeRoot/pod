#ifndef LINKS_H
#define LINKS_H

namespace links {
	static char const * const GAME_INFO = "https://api.rainfusion.ml/v1/ml/game/latest";
	static char const * const BETA_INFO = "https://api.rainfusion.ml/v1/ml/game/latest";
	static char const * const RAINFUSION_API_MODS = "https://api.rainfusion.ml/v1/db/items";
	static char const * const RAINFUSION_CDN_MOD = "https://cdn.rainfusion.ml/download-mod/";
	static char const * const RAINFUSION_ICON_BEFORE = "https://api.rainfusion.ml/v1/mod/";
	static char const * const RAINFUSION_ICON_AFTER = "/icon";
}

#endif // LINKS_H
