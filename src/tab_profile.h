#ifndef TAB_PROFILE_H
#define TAB_PROFILE_H

#include <profile.h>
#include <prefix.h>

#include <QWidget>
#include <QListView>

class tab_profile : public QWidget {
	Q_OBJECT

	public:
		tab_profile(QWidget * parent = nullptr);

		void setPrefix(prefix::Prefix current_prefix);
		void setProfile(profile::ProfileFile profile);
		void refreshProfiles();
		void createProfile(QString name);
		void deleteProfile(QString name);
		profile::ProfileFile getProfile(QString name);
		void refreshCurrentProfile();
		void selectedImportSave();
		void clickedBlankSave();

		std::string d_vanilla;
		prefix::Prefix current_prefix;
		std::vector<profile::ProfileFile> available_profiles;
		profile::ProfileFile current_profile;

	signals:
		void changedCurrentProfile(profile::ProfileFile current_profile);
		void changedProfiles();

	public slots:
		void profileSelected(QModelIndex const & index, QModelIndex const & parent);
		void createProfile();
		void deleteProfile();
		void createFlag();
		void deleteFlag();
		void editedFlag(QModelIndex const & topleft, QModelIndex const & bottomright, QVector<int> const & roles);

	private:
		bool saves_init();
		QListView * view_profiles = nullptr;
		QListView * view_flags = nullptr;
};

#endif //TAB_PROFILE_H
