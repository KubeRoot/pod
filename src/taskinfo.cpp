#include <taskinfo.h>

TaskInfo::TaskInfo() {}

TaskState TaskInfo::getState() {
	lock.lock();
	TaskState state = this->state;
	lock.unlock();
	return state;
}

bool TaskInfo::getCompleted() {
	lock.lock();
	TaskState state = this->state;
	lock.unlock();
	return (state == SUCCESS) || (state == FAILURE);
}

int TaskInfo::getProgress() {
	lock.lock();
	int value = progress;
	lock.unlock();
	return progress;
}

QString TaskInfo::getText() {
	lock.lock();
	QString value = text;
	lock.unlock();
	return text;
}

void TaskInfo::setState(TaskState state) {
	lock.lock();
	this->state = state;
	lock.unlock();
	emit progressChanged();
	if (state == SUCCESS)
		emit completed();
}

void TaskInfo::setProgress(int progress) {
	lock.lock();
	this->progress = progress;
	lock.unlock();
	emit progressChanged();
}

void TaskInfo::setText(QString text) {
	lock.lock();
	this->text = text;
	lock.unlock();
	emit progressChanged();
}

bool TaskInfo::shouldCancel() {
	lock.lock();
	TaskState state = this->state;
	lock.unlock();
	return state == CANCEL;
}
