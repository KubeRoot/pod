#ifndef TASKMODEL_H
#define TASKMODEL_H

#include <taskinfo.h>

#include <mutex>

#include <QString>
#include <QAbstractItemModel>
#include <QList>
#include <QItemDelegate>
#include <QtConcurrent>


class TaskModel : public QAbstractItemModel {
	Q_OBJECT

	public:
		TaskModel(QObject * parent = nullptr);
		~TaskModel();

		void clearCompleted();
		QModelIndex index(int row, int column, QModelIndex const & parent = QModelIndex()) const;
		QModelIndex parent(QModelIndex const & index) const;
		int rowCount(QModelIndex const & parent = QModelIndex()) const;
		int columnCount(QModelIndex const & parent = QModelIndex()) const;
		QVariant data(QModelIndex const & index, int role = Qt::DisplayRole) const;
		QVariant headerData(int section, Qt::Orientation orientation, int role) const;

		/*
		template <class C> TaskInfo * addTask(void (C::*task)(TaskInfo *), C * object);
		TaskInfo * addTask(void task(TaskInfo *));
		template <class C> TaskInfo * addTask(void (C::*task)(TaskInfo *), C * object) {
			emit layoutAboutToBeChanged();

			TaskInfo * taskinfo = new TaskInfo;
			connect(taskinfo, SIGNAL(progressChanged()), this, SLOT(progressUpdate()));
			QtConcurrent::run(object, task, taskinfo);
			tasks.push_back(taskinfo);

			emit layoutChanged();

			return taskinfo;
		}
		*/
		TaskInfo * addTask(TaskInfo * taskinfo);

		void wait();

	public slots:
		void progressUpdate();

	private:
		QList<TaskInfo *> tasks;
};

class TaskViewDelegate : public QItemDelegate {
	Q_OBJECT

	public:
		inline TaskViewDelegate(QWidget * parent = nullptr) : QItemDelegate(parent) {}
		void paint(QPainter * painter, QStyleOptionViewItem const & option, QModelIndex const & index) const;
};

#endif //FUTUREWATCHERMODEL_H
