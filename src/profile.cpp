#include <profile.h>

#include <util.h>

#include <QFile>
#include <QDir>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>

QJsonArray vectorToQJsonArray(std::vector<std::string> const & v) {
	QJsonArray array;
	for (int i = 0; i < v.size(); i ++) {
		array.push_back(QString::fromStdString(v.at(i)));
	}
	return array;
}

namespace profile {
	bool isProfileDirectory(std::string directory) {
		QString d_profile = QString::fromStdString(directory);
		return QDir(d_profile).exists() && QFile(d_profile + "/profile.json").exists();
	}

	profile_data::profile_data() {}

	ProfileFile::ProfileFile() {}

	ProfileFile::ProfileFile(QString f_profile, bool read_now) {
		this->f_profile = f_profile;
		if (read_now)
			read();
	}

	bool ProfileFile::write() {
		QFile file(f_profile);
		if (!file.open(QIODevice::WriteOnly))
			return false;

		QJsonObject profile_json;
		profile_json["mods"]  = QJsonArray(vectorToQJsonArray(data.enabled_mods));
		profile_json["flags"] = QJsonArray(vectorToQJsonArray(data.flags));

		file.write(QJsonDocument(profile_json).toJson());
		file.close();

		return true;
	}

	bool ProfileFile::read() {
		QFile file(f_profile);
		if (!file.open(QIODevice::ReadOnly))
			return false;

		QJsonObject object = QJsonDocument::fromJson(file.readAll()).object();

		file.close();

		data.enabled_mods = std::vector<std::string>();
		QJsonArray mods = object["mods"].toArray();
		for (int i = 0; i < mods.size(); i++) {
			data.enabled_mods.push_back(mods.at(i).toString().toStdString());
		}

		data.flags = std::vector<std::string>();
		QJsonArray flags = object["flags"].toArray();
		for (int i = 0; i < flags.size(); i++) {
			data.flags.push_back(flags.at(i).toString().toStdString());
		}
		
		return true;
	}

	bool ProfileFile::valid() {
		if (QFile(f_profile).exists() && QFileInfo(f_profile).fileName() == "profile.json")
			return true;
		return false;
	}

	QString ProfileFile::getName() {
		if (!valid())
			return "";

		return QFileInfo(util::getDirectory(f_profile)).fileName();
	}
}
