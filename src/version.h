#ifndef VERSION_H
#define VERSION_H

#include <string>
#include <vector>

namespace version {
	enum COMPARE {
		NOCOMPARE = -1,
		LESSER    = 0,
		EQUAL     = 1,
		GREATER   = 2
	};

	class IVersion {
		public:
			virtual bool isValid() const { return false; }
			virtual std::string toString() const { return std::string(); }
			virtual COMPARE compare(IVersion const * /* other */) const { return NOCOMPARE; }
	};

	class SemanticVersion : public IVersion {
		public:
			SemanticVersion();
			SemanticVersion(std::string version_string);

			bool isValid() const override;
			std::string toString() const override;
			COMPARE compare(IVersion const * other) const override;

			void fromString(std::string version_string);

			std::vector<int> numbers;
	};
}

#endif //VERSION_H
