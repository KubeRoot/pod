#ifndef VANILLA_H
#define VANILLA_H

#include <string>

namespace vanilla {
	bool isVanillaDirectory(std::string d_vanilla);
}

#endif //VANILLA_H
