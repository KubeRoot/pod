#include <vanilla.h>

#include <util.h>

namespace vanilla {
	bool isVanillaDirectory(std::string d_vanilla) {
		QString qd_vanilla = QString::fromStdString(d_vanilla);
		if (QFile(qd_vanilla + "/data.win").exists() && QFile(qd_vanilla + "/Risk of Rain.exe").exists())
			return true;
		return false;
	}

	bool isVanillaPath(std::string p_vanilla) {
		QString qd_vanilla = util::getDirectory(QString::fromStdString(p_vanilla));
		return isVanillaDirectory(qd_vanilla.toStdString());
	}
}
