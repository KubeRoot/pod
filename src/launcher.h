#ifndef LAUNCHER_H
#define LAUNCHER_H

#include <prefixmanager.h>
#include <prefix.h>
#include <profile.h>
#include <tab_mods.h>
#include <tab_profile.h>
#include <tab_tasks.h>
#include <tab_sources.h>
#include <tab_prefix.h>
#include <tab_logs.h>
#include <tab_home.h>

#include <vector>

#include <QMainWindow>
#include <QPushButton>
#include <QComboBox>

class Launcher : public QMainWindow {
	Q_OBJECT

	public:
		Launcher(QWidget * parent = nullptr);
		~Launcher();

		void refresh();
		void refreshLaunched();
		void refreshVanilla();

		void launch();
		void selectedProfile(int index);
		void changedCurrentProfile(profile::ProfileFile);
		void changedProfiles();
		void changedCurrentPrefix(QString d_prefix);
		void taskAdded(TaskInfo * info);

	private:
		prefix::Prefix current_prefix;
		PrefixManager * prefixmanager = nullptr;

		std::vector<QProcess *> launched_instances;

		QComboBox * combo_prefix = nullptr;
		QComboBox * combo_profile = nullptr;
		QLabel * label_launched = nullptr;
		QPushButton * button_launch = nullptr;
		tab_mods * mods = nullptr;
		tab_profile * profiles = nullptr;
		tab_tasks * tasks = nullptr;
		tab_sources * sources = nullptr;
		tab_prefix * prefix = nullptr;
		tab_logs * logs = nullptr;
		tab_home * home = nullptr;
};

#endif // LAUNCHER_H
