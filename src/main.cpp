#include <global.h>
#include <launcher.h>

#include <QApplication>

int main(int argc, char * * argv) {
	// Application
	QApplication app(argc, argv);

	// Getting and saving binary path
	global::setApplicationDirectory(app.applicationFilePath());
	qInfo() << "Application path is " << global::d_application;

	// Creating settings
	if (global::d_application == "/usr/bin") {
		global::settings = new QSettings(QSettings::UserScope, "pod");
	} else {
		global::settings = new QSettings(global::d_application + "/pod_settings.ini", QSettings::IniFormat);
		if (!global::settings->isWritable()) {
			delete global::settings;
			global::settings = new QSettings(QSettings::UserScope, "pod");
		}
	}

	//QCoreApplication::setOrganizationName("");
	//QCoreApplication::setOrganizationDomain("");
	QCoreApplication::setApplicationName("pod");

	// Run launcher
	Launcher launcher;
	launcher.show();
	return app.exec();
}
