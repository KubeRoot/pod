#include <tab_mods.h>

#include <algorithm>

#include <yaml-cpp/yaml.h>

// TODO
#include <QtCore>

#include <QFile>
#include <QDir>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMessageBox>
#include <QDesktopServices>

// Gets a string from the json representation
std::string getJsonString(YAML::Node const & node, std::string const & name) {
	std::string value;
	if (node[name])
		value = node[name].as<std::string>();
	return value;
}

// Regular JSON does not support trailing commas
// Most mods don't obey that
// I would use nlohmann::json Qt or otherwise
// Also some values might not load since the original launcher uses json.net which is case insensitive
mod::ModPath getModInfo(QString d_mod) {
	// TODO
	mod::ModPath m(d_mod.toStdString());
	m.refresh();
	return m;
}

// TODO Check metadata validity
bool isModDir(QString d_mod) {
	return QFile(d_mod + "/main.lua").exists() && QFile(d_mod + "/metadata.json").exists();
}

// Gets all valid mods in a directory
std::vector<mod::ModPath> getMods(QString d_mods) {
	std::vector<mod::ModPath> mods;

	QDir modsdir(d_mods);
	if (!modsdir.exists())
		return mods;

	QStringList mod_dirs = modsdir.entryList(QStringList(), QDir::Dirs);
	for (int i = 0; i < mod_dirs.size(); i++) {
		QString mod_dir = mod_dirs[i];
		if (mod_dir == "." || mod_dir == "..")
			continue;
		mod_dir = d_mods + "/" + mod_dir;
		if (isModDir(mod_dir)) {
			mod::ModPath mod = getModInfo(mod_dir);
			mods.push_back(mod);
		}
	}
	
	return mods;
}

void tab_mods::setProfile(profile::ProfileFile profile) {
	current_profile = profile;
	refreshMods();
}

void tab_mods::setPrefix(prefix::Prefix current_prefix) {
	this->current_prefix = current_prefix;
	refreshMods();
}

// Constructor
tab_mods::tab_mods(QWidget * parent) : QWidget(parent) {
	// Layout
	QHBoxLayout * main_layout = new QHBoxLayout;

	// Mod list view
	view_mods = new QTreeView;
	view_mods->setHeaderHidden(true);

	// Mod view (single mod description on the right side)
	mod_view = new ModView;
	
	// Selection mode (allows control, shift, dragging etc)
	view_mods->setSelectionMode(QAbstractItemView::ExtendedSelection);
	view_mods->setEditTriggers(QAbstractItemView::NoEditTriggers);

	// Sets view's model
	modlist_model = new ModListModel;
	view_mods->setModel(modlist_model);

	// Current mod in the right pane
	connect(view_mods->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)), this, SLOT(mod_activated(const QModelIndex &, const QModelIndex &)));

	// Button layout
	QVBoxLayout * button_layout = new QVBoxLayout();

	// Enable button
	QPushButton * button_enable = new QPushButton("Enable");
	connect(button_enable, SIGNAL(clicked()), this, SLOT(enableSelection()));
	button_layout->addWidget(button_enable);

	// Disable button
	QPushButton * button_disable = new QPushButton("Disable");
	connect(button_disable, SIGNAL(clicked()), this, SLOT(disableSelection()));
	button_layout->addWidget(button_disable);

	// Mods Dir button
	QPushButton * button_mods_dir = new QPushButton("Mods Dir");
	connect(button_mods_dir, SIGNAL(clicked()), this, SLOT(openModsDir()));
	button_layout->addWidget(button_mods_dir);
	
	QPushButton * button_refresh = new QPushButton("Refresh");
	connect(button_refresh, &QPushButton::clicked, this, &tab_mods::refreshMods);
	button_layout->addWidget(button_refresh);

	// Layout
	main_layout->addLayout(button_layout);
	main_layout->addWidget(view_mods);
	main_layout->addWidget(mod_view);
	this->setLayout(main_layout);

	// Load mods
	refreshMods();
}

bool contains(std::vector<std::string> const & v, std::string name) {
	for (int i = 0; i < v.size(); i++) {
		if (v.at(i) == name)
			return true;
	}
	return false;
}

// Enables selected mods and applies
void tab_mods::enableSelection() {
	if(profileError(true))
		return;

	std::vector<std::string> & enabled_mods = current_profile.data.enabled_mods;

	QList<QModelIndex> il = view_mods->selectionModel()->selectedIndexes();
	for (int i = 0; i < il.size(); i++) {
		std::string name = il.at(i).data().toString().toStdString();
		if (!contains(enabled_mods, name)) {
			enabled_mods.push_back(name);
		}
	}

	applyEnabled();
	view_mods->selectionModel()->clearSelection();
}

// Disables selected mods and applies
void tab_mods::disableSelection() {
	if (profileError(true))
		return;

	std::vector<std::string> & enabled_mods = current_profile.data.enabled_mods;

	QList<QModelIndex> il = view_mods->selectionModel()->selectedIndexes();
	for (int i = 0; i < il.size(); i++) {
		std::string name = il.at(i).data().toString().toStdString();
		if (contains(enabled_mods, name)) {
			enabled_mods.erase(std::find(enabled_mods.begin(), enabled_mods.end(), name));
		}
	}
	applyEnabled();
	view_mods->selectionModel()->clearSelection();
}

// Writes enabled mods to profile
void tab_mods::applyEnabled() {
	if (profileError(true))
		return;

	if (!current_profile.write())
		qInfo() << "Couldn't write to profile";

	refreshMods();
}

// Refreshes the mods, their enabled state and the view
void tab_mods::refreshMods() {
	if (profileError(false))
		return;

	mods = getMods(QString::fromStdString(current_prefix.path) + "/mods");

	current_profile.read();
	
	std::vector<bool> enabled;
	for (int i = 0; i < mods.size(); i++) {
		if (contains(current_profile.data.enabled_mods, mods.at(i).name))
			enabled.push_back(true);
		else
			enabled.push_back(false);
	}

	modlist_model->setMods(mods, enabled);
}

// Switches the displayed mod on the right pane
void tab_mods::mod_activated(QModelIndex const & index, QModelIndex const & previous_index) {
	QString name = index.data().toString();
	for (int i = 0; i < mods.size(); i++) {
		if (QString::fromStdString(mods.at(i).name) == name) {
			mod_view->setMod(mods.at(i));
			break;
		}
	}
}

bool tab_mods::profileError(bool box) {
	if (current_profile.valid())
		return false;
	if (box)
		QMessageBox::information(this, "Mods", "The current profile isn't valid, can't perform action");
	return true;
}

void tab_mods::openModsDir() {
	QDesktopServices::openUrl(QString::fromStdString(current_prefix.path) + "/mods");
}
