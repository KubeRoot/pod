#ifndef TAB_PREFIX_H
#define TAB_PREFIX_H

#include <prefix.h>
#include <prefixmanager.h>

#include <QWidget>
#include <QListView>
#include <QPushButton>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QList>
#include <QLineEdit>

class tab_prefix : public QWidget {
	Q_OBJECT

	public:
		tab_prefix();

		void selectedUpdate();
		void selectedCreate();
		bool setPrefixManager(PrefixManager * prefixmanager);
		bool refreshPrefixes();
		bool refreshVanilla();
		QList<QString> getPrefixes();

		void setPrefix(prefix::Prefix current_prefix);

	signals:
		void changedCurrentPrefix(QString d_prefix);

	public slots:
		void prefixSelected(QModelIndex const &, QModelIndex const &);

	private:
		prefix::Prefix current_prefix;
		QLineEdit * line_vanilla = nullptr;
		QListView * view_prefix = nullptr;
		QPushButton * button_update = nullptr;
		PrefixManager * prefixmanager = nullptr;
};

#endif //TAB_PREFIX_H
