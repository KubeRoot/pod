#include <tab_sources.h>

#include <links.h>
#include <rainfusion.h>
#include <mod.h>
#include <taskinfo.h>
#include <util.h>

#include <platform_quazip.h>

#include <regex>

#include <QBoxLayout>
#include <QNetworkReply>
#include <QStringListModel>
#include <QBuffer>
#include <QMessageBox>
#include <QDir>

tab_sources::tab_sources(QWidget * parent) : QWidget(parent) {
	netman = new QNetworkAccessManager;

	QHBoxLayout * layout_main = new QHBoxLayout(this);

	QVBoxLayout * layout_sources = new QVBoxLayout;
	QVBoxLayout * layout_mods = new QVBoxLayout;

	QStringListModel * model_sources = new QStringListModel;
	model_sources->setStringList(QStringList("Rainfusion"));

	view_sources = new QListView();
	view_sources->setModel(model_sources);
	view_sources->setEditTriggers(QAbstractItemView::NoEditTriggers);
	layout_sources->addWidget(view_sources);

	button_refresh = new QPushButton("Refresh");
	connect(button_refresh, SIGNAL(clicked()), this, SLOT(sourceRefreshBegin()));
	layout_sources->addWidget(button_refresh);

	QStringListModel * model_mods = new QStringListModel;
	view_mods = new QTreeView();
	view_mods->setHeaderHidden(true);
	view_mods->setModel(model_mods);
	view_mods->setSelectionMode(QAbstractItemView::ExtendedSelection);
	view_mods->setEditTriggers(QAbstractItemView::NoEditTriggers);
	connect(view_mods->selectionModel(), SIGNAL(currentChanged(const QModelIndex &, const QModelIndex &)), this, SLOT(selectedMod(const QModelIndex &, const QModelIndex &)));

	button_install = new QPushButton("Install");
	connect(button_install, &QPushButton::clicked, this, &tab_sources::selectedInstall);

	layout_mods->addWidget(view_mods);
	layout_mods->addWidget(button_install);

	view_mod = new ModView;

	layout_main->addLayout(layout_sources, 2);
	layout_main->addLayout(layout_mods,    4);
	layout_main->addWidget(view_mod,       4);
}

void tab_sources::sourceRefreshBegin() {
	if (!working) {
		working = true;
		button_refresh->setEnabled(false);
		QNetworkReply * reply = netman->get(QNetworkRequest(QUrl(links::RAINFUSION_API_MODS)));
		connect(reply, SIGNAL(finished()), this, SLOT(sourceRefreshRead()));
	}
}

void tab_sources::sourceRefreshRead() {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply->error()) {
		QString html = reply->readAll();
		std::vector<rainfusion::RainfusionMod> mods = rainfusion::parseMods(html.toStdString());
		QStringList modlist;
		for (size_t i = 0; i < mods.size(); i++) {
			auto mod = mods.at(i);
			modlist << QString::fromStdString(mod.name);
			std::string url = links::RAINFUSION_ICON_BEFORE + mod.uuid + links::RAINFUSION_ICON_AFTER;
			QString url_qstring = QString::fromStdString(url);
			QNetworkReply * reply = netman->get(QNetworkRequest(QUrl(url_qstring)));
			connect(reply, SIGNAL(finished()), this, SLOT(iconDownloadRead()));
			icon_downloads.insert({reply, mod});
		}
		qobject_cast<QStringListModel *>(view_mods->model())->setStringList(modlist);
		this->mods = mods;
	}
	reply->deleteLater();
	working = false;
	button_refresh->setEnabled(true);
}

void tab_sources::iconDownloadRead() {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	auto find_download = icon_downloads.find(reply);
	if (!reply->error()) {
		auto mod = (*find_download).second;
		QPixmap icon;
		icon.loadFromData(reply->readAll());
		auto element = std::pair<std::string, QPixmap>(mod.uuid, icon);
		icons.insert(element);
	}
	icon_downloads.erase(find_download);
	reply->deleteLater();
}

rainfusion::RainfusionMod tab_sources::getMod(QString name) {
	for (size_t i = 0; i < mods.size(); i++) {
		if (QString::fromStdString(mods.at(i).name) == name) {
			return mods.at(i);
		}
	}
	rainfusion::RainfusionMod mod;
	return mod;
}

void tab_sources::selectedMod(QModelIndex const & index, QModelIndex const & previous_index) {
	QString name = index.data().toString();
	rainfusion::RainfusionMod mod = getMod(name);
	view_mod->setMod(mod);
	if (icons.count(mod.uuid) > 0) {
		view_mod->setIcon(icons.at(mod.uuid));
	}
}

void tab_sources::selectedInstall() {
	// TODO
	if (!current_prefix.valid() || !prefix::isPrefixDirectory(current_prefix.path))
		return;

	QList<QModelIndex> selected = view_mods->selectionModel()->selectedIndexes();
	if (selected.size() <= 0)
		return;

	QList<QString> confirmed;
	
	for (size_t i = 0; i < selected.size(); i++) {
		QString name = selected.at(i).data().toString();
		QString install_path = QString::fromStdString(current_prefix.path + "/mods/" + util::cleanString(name.toStdString()));
		if (QDir(install_path).exists()) {
			rainfusion::RainfusionMod mod = getMod(name);
			mod::ModPath installed = mod::getMod(install_path.toStdString());
			if (mod.version.compare(&installed.version) != version::GREATER) {
				auto answer = QMessageBox::question(this, "Install Mod", QString("Overwrite the installed mod '" + name + "' that has the same or higher version? (%1->%2)").arg(QString::fromStdString(installed.version.toString()), QString::fromStdString(mod.version.toString())), QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
				switch (answer) {
					case QMessageBox::Cancel:
						return;
					case QMessageBox::Yes:
						break;
					case QMessageBox::No:
						continue;
					default:
						QMessageBox::information(this, "Install Mod", "Bug?");
						return;
				}
			}
		}
		confirmed.push_back(name);
	}

	if (confirmed.count() <= 0)
		return;

	for (size_t i = 0; i < confirmed.size(); i++) {
		QString name = confirmed.at(i);
		rainfusion::RainfusionMod mod = getMod(name);

		if (!QUrl(QString::fromStdString(mod.download)).isValid())
			continue;

		TaskInfo * info = new TaskInfo;
		info->setText("Downloading mod: " + name);
		info->setProgress(0);
		info->setState(RUNNING);

		QNetworkReply * reply = netman->get(QNetworkRequest(QUrl(QString::fromStdString(mod.download))));
		connect(reply, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(download_error(QNetworkReply::NetworkError)));
		connect(reply, SIGNAL(finished()), this, SLOT(download_finished()));
		connect(reply, SIGNAL(downloadProgress(qint64, qint64)), this, SLOT(download_progress(qint64, qint64)));

		modl m;
		m.task = info;
		m.name = mod.name;
		m.path = util::cleanString(mod.name);
		downloads[reply] = m;

		emit taskAdded(info);
	}

	QMessageBox::information(this, "Install Mod", "Tasks added");
}

void tab_sources::download_error(QNetworkReply::NetworkError) {
	qInfo() << "Couldn't download mod";
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply)
		return;
	if (!downloads.count(reply))
		return;
	modl m = downloads.at(reply);
	m.task->setText("Download failed: " + QString::fromStdString(m.name));
	m.task->setProgress(100);
	m.task->setState(FAILURE);
	downloads.erase(downloads.find(reply));
	reply->deleteLater();
}

void tab_sources::download_progress(qint64 received, qint64 total) {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply)
		return;
	int progress = (int)((double)received * 100/total);
	if (!downloads.count(reply))
		return;
	modl m = downloads.at(reply);
	m.task->setProgress(progress);
}

void tab_sources::download_finished() {
	QNetworkReply * reply = qobject_cast<QNetworkReply *>(sender());
	if (!reply)
		return;

	if (!downloads.count(reply))
		return;
	modl m = downloads.at(reply);

	if (reply->error() != QNetworkReply::NoError) {
		m.task->setState(FAILURE);
		downloads.erase(downloads.find(reply));
		reply->deleteLater();
		return;
	}

	m.task->setText("Extracting: " + QString::fromStdString(m.name));

	QBuffer * buffer = new QBuffer;
	buffer->setData(reply->readAll());

	// TODO
	QString d_mods = QString::fromStdString(current_prefix.path) + "/mods";

	QString path = d_mods + "/" + QString::fromStdString(m.path);

	QuaZip zip;
	zip.setIoDevice(buffer);
	zip.open(QuaZip::mdUnzip);

	if (zip.setCurrentFile("metadata.json")) {
		// TODO Error
		if (!QDir(path).removeRecursively())
			return;
		if (!QDir().mkpath(path))
			return;
		JlCompress::extractDir(buffer, path);
	} else {
		QuaZipDir dir(&zip, "/");
		// These have slashes at the end
		QList<QString> entries = dir.entryList();
		QList<QString> subdirs;
		for (size_t i = 0; i < entries.size(); i++) {
			if (zip.setCurrentFile(entries.at(i) + "metadata.json")) {
				subdirs.push_back(entries.at(i));
			}
		}
		int mod_count = subdirs.count();
		if (mod_count > 0) {
			QList<QString> files = zip.getFileNameList();
			int file_count = files.size();
			zip.close();
			for (size_t i = 0; i < mod_count; i++) {
				m.task->setProgress(0);
				QString subdir = subdirs.at(i);
				if (mod_count != 1)
					path = d_mods + "/" + subdir;
				// TODO Error
				if (!QDir(path).removeRecursively())
					return;
				if (!QDir().mkpath(path))
					return;
				for (size_t i = 0; i < files.size(); i++) {
					//int pos = files.at(i).indexOf(subdir);
					//if (pos == 0) {
					if (files.at(i).startsWith(subdir)) {
						QString name = files.at(i);
						name = name.right(name.size() - subdir.size());
						JlCompress::extractFile(buffer, files.at(i), path + "/" + name);
					}
					int progress = (int)((double)i * 100/file_count);
					m.task->setProgress(progress);
				}
			}
		} else {
			qInfo() << "Couldn't find metadata in mod";
		}
	}

	m.task->setProgress(100);
	m.task->setText("Installed mod: " + QString::fromStdString(m.name));
	m.task->setState(SUCCESS);
	downloads.erase(downloads.find(reply));
	reply->deleteLater();

	emit changedMods();
}

bool tab_sources::setPrefix(prefix::Prefix current_prefix) {
	this->current_prefix = current_prefix;
	return true;
}
