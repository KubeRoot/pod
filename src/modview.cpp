#include <modview.h>

#include <QTextDocument>
#include <QBoxLayout>

#include <QPixmap>

ModView::ModView(QWidget * parent) : QWidget(parent) {
	QVBoxLayout * layout_main = new QVBoxLayout;

	QHBoxLayout * layout_top = new QHBoxLayout;

	label_title = new QLabel("...");

	label_icon = new QLabel;
	label_icon->setAlignment(Qt::AlignRight);
	label_icon->setMinimumSize(100, 100);
	label_icon->setMaximumSize(100, 100);
	label_icon->setScaledContents(true);

	layout_top->addWidget(label_title);
	layout_top->addWidget(label_icon);

	text_description = new QTextEdit;
	text_description->setReadOnly(true);

	layout_main->addLayout(layout_top);
	layout_main->addWidget(text_description);

	this->setLayout(layout_main);
}

QString fs(std::string s) {
	return QString::fromStdString(s);
}

void infoToTextEdit(mod::IMod const & mod, mod::INFO info_type, QTextEdit * edit) {
	QString text = fs(mod.getInfo(info_type));
	switch (mod.getTextType(info_type)) {
		case mod::PLAIN:
			edit->setPlainText(text);
			break;
		case mod::HTML:
			edit->setHtml(text);
			break;
		// TODO Compile Qt 5.14.1
		/*
		case mod::MARKDOWN:
			edit->setMarkdown(text);
			break;
		*/
		default:
			edit->setPlainText(text);
			break;
	}
}

void ModView::setMod(mod::IMod const & mod) {
	std::string text;

	std::string name        = mod.getInfo(mod::NAME);
	std::string version     = mod.getInfo(mod::VERSION);
	std::string author      = mod.getInfo(mod::AUTHOR);
	std::string description = mod.getInfo(mod::DESCRIPTION);

	if (name != "\0")
		text += "Name: " + name + "\n";
	if (version != "\0")
		text += "Version: " + version + "\n";
	if (author != "\0")
		text += "Author: " + author + "\n";

	label_title->setText(fs(text));

	// TODO
	/*
	QPixmap pix;
	QString f_icon = prefix_of_mod + "/mods/" + fs(name) + "icon.png";
	if (QFile(f_icon).exists()) {
		pix = QPixmap(f_icon);
		label_icon->setPixmap(QPixmap(f_icon));
		label_icon->setVisible(true);
	} else {
		label_icon->setVisible(false);
	}
	*/

	infoToTextEdit(mod, mod::DESCRIPTION, text_description);
}

void ModView::setIcon(QPixmap icon) {
	label_icon->setPixmap(icon);
}
