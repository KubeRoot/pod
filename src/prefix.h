#ifndef PREFIX_H
#define PREFIX_H

#include <mod.h>
#include <profile.h>

namespace prefix {
	bool isPrefixDirectory(std::string directory);

	// Profiles
	profile::ProfileFile getProfile(std::string d_prefix, std::string profile_name);
	std::vector<profile::ProfileFile> getProfiles(std::string d_prefix);
	bool createProfile(std::string d_prefix, std::string profile_name);
	bool deleteProfile(std::string d_prefix, std::string profile_name);

	// Current Profile
	bool setCurrentProfile(std::string d_prefix, std::string profile_name);
	profile::ProfileFile getCurrentProfile(std::string d_prefix);

	std::string getPrefixVersionString(std::string d_prefix);

	class Prefix {
		public:
			Prefix();
			Prefix(std::string path);

			bool valid() const;
			bool refresh();
			bool setPath(std::string path);

			profile::ProfileFile getProfile(std::string profile_name);
			bool createProfile(std::string profile_name);
			bool deleteProfile(std::string profile_name);

			bool setCurrentProfile(std::string profile_name);
			profile::ProfileFile getCurrentProfile();

			bool readProfiles();

			std::string path = "\0";
			//std::vector<mod::ModPath> mods;
			std::vector<profile::ProfileFile> profiles;
	};
}

#endif //PREFIX_H
