#include <tab_logs.h>

#include <QHBoxLayout>

tab_logs::tab_logs() {
	QHBoxLayout * layout_main = new QHBoxLayout;

	view_logs = new QTextEdit(this);
	view_logs->setReadOnly(true);

	layout_main->addWidget(view_logs);

	setLayout(layout_main);
}

void tab_logs::log(QString text) {
	view_logs->moveCursor(QTextCursor::End);
	view_logs->insertPlainText(text);
}
