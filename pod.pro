### Debug / Release ###

	CONFIG += debug
	# Uncomment the next line to build release (Last one takes effect)
	CONFIG += release


### Target ###

	DESTDIR = build
	TARGET = pod


### Object ###

	OBJECTS_DIR = obj
	RCC_DIR = obj
	MOC_DIR = obj


### Modules ###

	QT += core gui widgets network concurrent


### quazip ###

	#unix:LIBS += -lquazip5
	# For compiling quazip
	LIBS += -lz
	QMAKE_CXXFLAGS += -DQUAZIP_STATIC
	unix:HEADERS += lib/quazip/quazip/*.h
	unix:SOURCES += lib/quazip/quazip/*.cpp
	unix:SOURCES += lib/quazip/quazip/*.c
	unix:INCLUDEPATH += lib/quazip/

	# MXE quazip
	win32:LIBS += $$system(i686-w64-mingw32.static-pkg-config quazip --cflags --libs --static)
	win32:QMAKE_CXXFLAGS += $$system(i686-w64-mingw32.static-pkg-config quazip --cflags --static)


### Flags ###

	QMAKE_CXXFLAGS += -std=c++11
	QMAKE_CXXFLAGS += -Werror=return-type

	# Fixes some distros not launching on double-click
	unix:QMAKE_LFLAGS += -no-pie


### yaml-cpp ###

	INCLUDEPATH += lib/yaml-cpp/include
	SOURCES += ./lib/yaml-cpp/src/*.cpp
	HEADERS += ./lib/yaml-cpp/src/*.h
	HEADERS += ./lib/yaml-cpp/include/yaml-cpp/*.h


### tinyxml2 ###

	INCLUDEPATH += lib/tinyxml2
	SOURCES += ./lib/tinyxml2/tinyxml2.cpp
	HEADERS += ./lib/tinyxml2/tinyxml2.h


### Sources ###

	INCLUDEPATH += src

	SOURCES += src/*.cpp
	HEADERS += src/*.h


### Resources ###

	RC_ICONS = res/icon.ico
	RESOURCES += res/icon.qrc
