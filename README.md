# Pod

Pod is a custom launcher for Risk of Rain Modloader

## Dependencies

* Qt5
* Wine (For running on platforms other than Windows)

## Compiling

For all platforms the generated program should be in `build/`  
All commands in this section should be run on the project root where the `.pro` file is  
Make sure you also get the submodules with this command first:  
```bash
git submodule update --init --recursive
```

### Linux

```bash
qmake && make
```

### macOS

```bash
qmake && make
```

### Windows from Linux

Setup [MXE](https://mxe.cc)  
```bash
<MXE Directory>/usr/<TARGET>/qt5/bin/qmake
make
```

### Windows from Windows

The current build system uses MXE but the `.pro` file could be modified to make it possible.  
Make sure the PATH is set correctly for Qt compilation.  
Commands should be something like:  
```bash
qmake
mingw32-make
```

You might also need windeplotqt.exe if you're building dynamically.  
```bash
cd build/ && windeployqt.exe && cd ..
```

## Notes

### Setting Up
This launcher needs the Windows version of Risk of Rain to work. To get it you can use SteamCMD or force proton (wine) from Steam (if you have Risk of Rain on Steam).

### Reload
There are some cases where the launcher does not reload properly, such as after the setup. Start it again to see if the problem persists.

### Saves
Saves are not imported by default. Therefore progress is not saved until a new save is created or a vanilla save is imported. Both can be done under the Profiles tab in the launcher.

## Contact

If you have a problem or would like to discuss something:
* Discord none#3549
* Email <nonena@protonmail.com>
* [Risk of Rain 1 Discord Server](https://discord.com/invite/ajsGTdN)
