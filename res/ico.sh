#!/bin/sh

convert icon.png -filter point -resize 256x256 -define icon:auto-resize="256,128,96,64,48,32,16" icon.ico
